package com.example.contactmanager.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.contactmanager.models.ContactModel;

import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "contact.db";
    public static final String TABLE_NAME = "contact_table";
    public static final String CONTACT_ID = "id";
    public static final String CONTACT_NAME = "name";
    public static final String CONTACT_ADDRESS = "address";
    public static final String CONTACT_PHONE_NUMBER = "number";
    private HashMap hp;
    private String CREATE_TABLE_CONTACT_TABLE = "create table " + TABLE_NAME + "(" + CONTACT_ID + " integer primary key autoincrement, "
            + CONTACT_NAME + " text, " + CONTACT_ADDRESS + " text, " + CONTACT_PHONE_NUMBER + " text)";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(CREATE_TABLE_CONTACT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertContact(String name, String address, String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("address", address);
        contentValues.put("number", number);
        db.insert("contact_table", null, contentValues);
        return true;
    }

    public Cursor getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from contact_table where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public boolean updateContact(Integer id, String name, String address, String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("address", address);
        contentValues.put("phone", phone);
        db.update("contact_table", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer deleteProduct(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contact_table",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<ContactModel> getAllContact() {
        ArrayList<ContactModel> array_list = new ArrayList<ContactModel>();

        SQLiteDatabase db = getReadableDatabase();
        String queryGet = "select " + CONTACT_ID + "," + CONTACT_NAME + "," + CONTACT_ADDRESS + "," + CONTACT_PHONE_NUMBER  + " from " + TABLE_NAME;
        Cursor c = null;
        try {
            c = db.rawQuery(queryGet, null);
            if (c.moveToFirst()) {
                for (int i = 0; i < c.getCount(); i++) {
                    ContactModel acc = new ContactModel();
                    acc.setContactId(c.getString(c.getColumnIndex(CONTACT_ID)));
                    acc.setContactName(c.getString(c.getColumnIndex(CONTACT_NAME)));
                    acc.setContactAddress(c.getString(c.getColumnIndex(CONTACT_ADDRESS)));
                    acc.setContactPhone(c.getString(c.getColumnIndex(CONTACT_PHONE_NUMBER)));

                    array_list.add(acc);
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            Log.e(" getAllAccounts ex", e.getMessage() + " ");
        } finally {
            if (c != null) {
                c.close();
            }
            SQLiteDatabase.releaseMemory();
        }
        return array_list;
    }

    public void clearDb() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_NAME, null, null);
        } catch (Exception e) {

        }
        db.close();
    }
}
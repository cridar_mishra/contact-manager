package com.example.contactmanager.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.contactmanager.R;
import com.example.contactmanager.helpers.DBHelper;

public class AddContactActivity extends AppCompatActivity {
    DBHelper dbHelper;
    EditText etContactName, etContactAddress, etContactPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView customTitle = findViewById(R.id.custom_toolbar_title);
        customTitle.setText("Add Contact");

        dbHelper = new DBHelper(this);

        Button btnAdd = findViewById(R.id.btn_add);
        Button btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                onBackPressed();
            }
        });
        etContactName = findViewById(R.id.et_name);
        etContactAddress = findViewById(R.id.et_address);
        etContactPhone = findViewById(R.id.et_number);

        etContactAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etContactAddress.getText().toString().length() == 100) {
                    Toast.makeText(AddContactActivity.this, "Address field cannot exceed 100 characters", Toast.LENGTH_SHORT).show();
                }

            }
        });

        etContactPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (etContactPhone.getText().toString().isEmpty()) {
                    if (b) {
                        etContactPhone.setText("+");
                        etContactPhone.setSelection(1);
                    }
                }
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etContactName.getText().toString().trim())) {
                    Toast.makeText(getBaseContext(), "Contact Name is empty.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etContactAddress.getText().toString().trim())) {
                    Toast.makeText(getBaseContext(), "Please Provide Address", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etContactPhone.getText().toString().trim())) {
                    Toast.makeText(getBaseContext(), "Please Provide Contact Number", Toast.LENGTH_SHORT).show();
                } else {
                    if (dbHelper.insertContact(etContactName.getText().toString().trim()
                            , etContactAddress.getText().toString().trim(),
                            etContactPhone.getText().toString().trim())) {
                        Toast.makeText(getBaseContext(), "Contact Added.", Toast.LENGTH_SHORT).show();
                        etContactName.setText("");
                        etContactAddress.setText("");
                        etContactPhone.setText("");
                    } else {
                        Toast.makeText(getBaseContext(), "Product Added.", Toast.LENGTH_SHORT).show();
                    }
                }
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            }
        });

    }
}
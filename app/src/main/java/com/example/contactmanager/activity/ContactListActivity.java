package com.example.contactmanager.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.contactmanager.R;
import com.example.contactmanager.adapter.ContactListAdapter;
import com.example.contactmanager.helpers.DBHelper;
import com.example.contactmanager.models.ContactModel;

import java.util.ArrayList;


public class ContactListActivity extends AppCompatActivity {

    DBHelper dbHelper;
    ContactListAdapter contactListAdapter;
    ArrayList<ContactModel> contactList;
    TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        dbHelper = new DBHelper(this);


        tvError = findViewById(R.id.error);
        RelativeLayout buttonAddContact = findViewById(R.id.btn_add_contact);
        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactListActivity.this, AddContactActivity.class);
                startActivity(intent);
            }
        });

        getData();
    }

    private void getData() {

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        contactList = dbHelper.getAllContact();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        contactListAdapter = new ContactListAdapter(this, contactList);
        recyclerView.setAdapter(contactListAdapter);
        recyclerView.setVisibility(View.VISIBLE);

        if (contactList.size() < 1) {
            tvError.setVisibility(View.VISIBLE);
        } else {
            tvError.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    public View getTextView() {
        return tvError;
    }
}
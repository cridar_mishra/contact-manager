package com.example.contactmanager.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactmanager.R;
import com.example.contactmanager.activity.ContactListActivity;
import com.example.contactmanager.helpers.DBHelper;
import com.example.contactmanager.models.ContactModel;

import java.util.ArrayList;

public class ContactListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ContactModel> productList;
    DBHelper dbHelper;

    public ContactListAdapter(Context context, ArrayList<ContactModel> kharchaList) {
        this.productList = kharchaList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        ViewHolder viewHolder = (ViewHolder) holder;
        ContactModel contactModel = productList.get(i);
        viewHolder.tvName.setText(contactModel.getContactName());
        viewHolder.tvAddress.setText(contactModel.getContactAddress());
        viewHolder.tvNumber.setText(contactModel.getContactPhone());


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvAddress, tvNumber;
        ImageView btnDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvNumber = itemView.findViewById(R.id.tv_number);
            btnDelete = itemView.findViewById(R.id.btn_delete);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure to delete this contact?");
                    alertDialogBuilder.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                    dbHelper = new DBHelper(context);
                                    dbHelper.deleteProduct(Integer.parseInt(productList.get(getLayoutPosition()).getContactId()));
                                    productList.remove(getLayoutPosition());
                                    notifyDataSetChanged();
                                    arg0.dismiss();
                                    Toast.makeText(context, "contact deleted", Toast.LENGTH_SHORT).show();
                                    if (productList.size() < 1) {
                                        ((ContactListActivity) context).getTextView().setVisibility(View.VISIBLE);
                                    } else {
                                        ((ContactListActivity) context).getTextView().setVisibility(View.GONE);
                                    }
                                }
                            });

                    alertDialogBuilder.setNegativeButton("cancel",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                    alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);

                }
            });

        }
    }
}
